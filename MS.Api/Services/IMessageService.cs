﻿namespace MS.Api.Services
{
    public interface IMessageService
    {
        bool Enqueue(string messageString);
    }
}